FROM openjdk:12

WORKDIR /tmp

COPY ./src/Main.java .

RUN javac Main.java

CMD ["java", "Main"]
